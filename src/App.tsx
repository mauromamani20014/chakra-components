import { FC } from 'react';

import { Ecommerce } from './components/ecommerce';

const App: FC = () => {
  return <Ecommerce />;
};

export default App;
