import { Box, Heading, Image, Link, Stack, Text } from '@chakra-ui/react';

export const Card = () => {
  const CardComponent = () => {
    return (
      <Box
        width="23rem"
        mt="4rem"
        border="1px"
        borderColor="gray.200"
        borderRadius="lg"
        transition="all 0.4s"
        _hover={{ shadow: 'xl' }}>
        <Box>
          <Image
            src="https://images.unsplash.com/photo-1505944270255-72b8c68c6a70?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8ZmFjaWFsfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
            borderTopRadius="lg"
            objectFit="fill"
          />
        </Box>
        <Box padding="5">
          <Stack direction="column">
            <Text fontSize="sm" textColor="gray.500" fontWeight="semibold">
              FASHION
            </Text>
            <Heading fontSize="xl">Lorem ipsum dolor sit.</Heading>
            <Text textColor="gray.600">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas.
            </Text>
          </Stack>
          <Box
            marginTop="1rem"
            display="flex"
            flexDirection="row"
            justifyContent="space-between"
            textColor="gray.600">
            <Text>
              By{' '}
              <Link href="go" textDecoration="underline">
                Lorem
              </Link>
            </Text>
            <Text fontSize="sm">🕑 3 min read</Text>
          </Box>
        </Box>
      </Box>
    );
  };

  return (
    <Box display="flex" justifyContent="center">
      <CardComponent />
    </Box>
  );
};
