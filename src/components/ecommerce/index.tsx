import { Box, Button, Heading, Image, Stack, Text } from '@chakra-ui/react';

import { HeaderWithActions } from '../headers/HeaderWithActions';

export const Ecommerce = () => {
  return (
    <Box backgroundColor="gray.200">
      <HeaderWithActions />
      <Box display="flex" paddingTop="2rem" flexDirection={{ base: 'column', md: 'row' }}>
        <Box
          width={{ base: '100%', md: '50%' }}
          display="flex"
          justifyContent="center"
          paddingX={{ md: '1rem' }}>
          <Image
            boxSize={{ base: '90%', md: '400', lg: '500' }}
            boxShadow="xl"
            rounded="xl"
            src="https://assets.adidas.com/images/w_600,f_auto,q_auto/f77c0cc30581440a9996ac4400666419_9366/Go-To_Crewneck_Sweatshirt_Blue_GM0036_21_model.jpg"
          />
        </Box>
        <Stack
          direction={['column']}
          spacing="8px"
          width={{ base: '100%', md: '50%' }}
          paddingTop={{ base: '1.5rem', md: '0' }}
          paddingX="2rem">
          <Heading fontSize={{ base: '2xl', md: '4xl' }}>
            Men&apos;s Chill Crew Neck Sweatshirt
          </Heading>
          <Text fontSize={{ base: 'lg', md: 'xl' }} fontWeight="bold">
            $75
          </Text>
          <Box paddingY="8px">
            <Text fontSize="sm" fontWeight={'semibold'} paddingBottom={'1rem'}>
              Cantidad
            </Text>
            <Button rounded="full" colorScheme="blue" width={{ base: '100%', lg: '50%' }}>
              Agregar al carrito
            </Button>
          </Box>
          <Box>
            <Heading fontSize="xl">Description</Heading>
            <Text>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore tempora
              ullam iure consequuntur natus vitae vel repellat omnis tenetur vero!
            </Text>
          </Box>
        </Stack>
      </Box>
    </Box>
  );
};
