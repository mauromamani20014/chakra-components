import { MoonIcon } from '@chakra-ui/icons';
import { Box, Flex, Heading, Spacer, Stack, Text } from '@chakra-ui/react';
import { FC } from 'react';

export const BoxComponent: FC = () => {
  const NavItem: FC<{ title: string }> = ({ title }) => {
    return (
      <Box
        mr={2}
        px={4}
        py={2}
        _hover={{ textDecoration: 'none', bgColor: 'gray.800' }}
        fontSize="sm"
        cursor="pointer"
        rounded="sm">
        {title}
      </Box>
    );
  };

  const Jumbotron: FC = () => {
    return (
      <Stack
        direction={['column']}
        spacing={8}
        color="gray.100"
        textAlign="center"
        alignItems="center"
        justifyContent="center">
        <Box>
          <Heading
            as="h1"
            size="2xl"
            bgGradient="linear(to-l, #79c2ff, #4a5888)"
            bgColor="blue.400"
            bgClip="text">
            I&apos;m a Heading
          </Heading>
        </Box>
        <Box>
          <Heading as="h2" size="lg" lineHeight="tall">
            <Box as="span">Lorem ipsum</Box>
            dolor sit amet consectetur adipisicing elit.
            <Box as="span"> Neque, laborum Lorem</Box>
          </Heading>
        </Box>
      </Stack>
    );
  };

  return (
    <Box h="100vh" bg="gray.900" mx="auto" borderTopWidth={5} borderColor="blue.400">
      {/* navbar */}
      <Flex w="85%" mx="auto" p={3} color="gray.100" align="center" as="header">
        <Box>
          <MoonIcon w="30px" h="30px" />
        </Box>
        <Spacer />
        <Stack direction={['row']}>
          <NavItem title="Login" />
          <NavItem title="Register" />
          <NavItem title="About" />
        </Stack>
      </Flex>

      <Box
        d="flex"
        px={6}
        maxW="2xl"
        mx="auto"
        height={['calc(100vh - 66px)']}
        justifyContent="center"
        alignItems="center">
        <Jumbotron />
      </Box>

      <Box bg="gray.900" height="100%" px={10} color="gray.100">
        <Box maxW="2xl" mx="auto">
          <Box d="flex" justifyContent="space-between" alignItems="end">
            <Heading as="h2" size="xl">
              Articles
            </Heading>
            <Text as="a" href="#">
              View articles
            </Text>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
