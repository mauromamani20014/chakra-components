import { Box, Button, Flex, Heading, Input, Link, Stack, Text } from '@chakra-ui/react';

export const Authentication = () => {
  return (
    <Flex h="100vh" direction={{ base: 'column', md: 'row' }}>
      <Box
        bg="blue.500"
        w={{ base: '100%', md: '40%' }}
        py={5}
        px={20}
        color="gray.100"
        display={{ base: 'none', md: 'block' }}>
        <Heading as="h2" size="lg">
          Envelope
        </Heading>
        <Flex
          height={['calc(100vh - 60px)']}
          direction="column"
          maxW="78%"
          textAlign="center"
          justify="center"
          align="center">
          <Heading
            as="h1"
            size="2xl"
            fontWeight="black"
            letterSpacing="tight"
            lineHeight="normal">
            Introducing our 2020 report
          </Heading>
          <Text mt="5">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididun.
          </Text>
          <Button mt="5" w="50%" variant="outline">
            View Report
          </Button>
        </Flex>
      </Box>
      <Box
        bg="gray.100"
        w={{ base: '100%', md: '60%' }}
        h={{ base: '100%' }}
        py={5}
        px={20}>
        <Flex direction="column" height="full" justify="center">
          <Box mb="10">
            <Heading fontSize="5xl" color="blue.500" fontWeight="black">
              Welcome Back
            </Heading>
            <Heading fontSize="3xl" color="gray.500">
              Sign in to continue
            </Heading>
          </Box>

          <Box bgColor="white" p="10" rounded="lg" shadow="xl">
            <Stack direction={['column']} spacing="7">
              <Input placeholder="Email"></Input>
              <Input placeholder="Password"></Input>
            </Stack>
            <Link
              href="www.google.com"
              display="block"
              mt="3"
              fontSize="sm"
              color="blue.500"
              fontWeight="semibold">
              Forgot Password?
            </Link>

            <Flex justify="space-between" align="center">
              <Text>
                New User?{' '}
                <Link href="www.c.com" color="blue.500" fontWeight="semibold">
                  Create Account
                </Link>
              </Text>

              <Button colorScheme="blue" p="6">
                Sign in
              </Button>
            </Flex>
          </Box>
        </Flex>
      </Box>
    </Flex>
  );
};
