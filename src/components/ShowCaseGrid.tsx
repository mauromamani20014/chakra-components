// https://pro.chakra-ui.com/components/e-commerce/category-showcases

import { Box, Flex, Grid, GridItem, Heading, Image, Link } from '@chakra-ui/react';
import { FC } from 'react';

export const ShowCaseGrid: FC = () => {
  return (
    <Box py="12" px="20">
      <Flex direction="row" justify="space-between" align="end">
        <Heading as="h2" fontSize="3xl">
          Shop by Categories
        </Heading>

        <Link as="a" fontWeight="semibold" color="blue.500">
          See all categories ➜
        </Link>
      </Flex>

      <Box mt="12" h="100vh">
        <Grid templateColumns={{ md: 'repeat(3, 1fr)', lg: 'repeat(4, 1fr)' }} gap={6}>
          {[1, 2, 3, 4, 5, 6, 7, 8].map((n) => (
            <GridItem w="100%" h="80" bg="blue.500" key={n} rounded="2xl">
              <Box position="relative" w="full" h="full">
                <Image
                  src="https://www.dellacasaonline.com/wp-content/uploads/2020/10/barcelona-1.jpg"
                  objectFit="fill"
                  rounded="2xl"
                  boxSize="full"
                />
                <Heading
                  color="white"
                  fontSize="lg"
                  position="absolute"
                  transform="auto"
                  translateX="120px"
                  translateY="-70px">
                  ASDA
                </Heading>
              </Box>
            </GridItem>
          ))}
        </Grid>
      </Box>
    </Box>
  );
};
