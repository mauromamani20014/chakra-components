/* eslint-disable react/no-children-prop */
import { ChevronDownIcon, SearchIcon } from '@chakra-ui/icons';
import {
  Box,
  Button,
  Heading,
  Input,
  InputGroup,
  InputRightElement,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Text,
} from '@chakra-ui/react';

export const HeaderWithActions = () => {
  return (
    <Box padding="1rem">
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Box>
          <Heading fontSize="3xl">Vendor List</Heading>
          <Text fontSize="sm">Count: 1200 contacts</Text>
        </Box>
        <Stack direction={['row']} spacing="4">
          <InputGroup>
            <InputRightElement children={<SearchIcon color="gray.400" />} />
            <Input placeholder="Search for content" backgroundColor="gray.50" />
          </InputGroup>
          <Menu>
            <MenuButton
              width="9rem"
              as={Button}
              rightIcon={<ChevronDownIcon ml="3.3rem" />}
              colorScheme="blue"
              variant="outline">
              Actions
            </MenuButton>
            <MenuList>
              <MenuItem>Re-order columns</MenuItem>
              <MenuItem>Add new properties</MenuItem>
              <MenuItem>Import list</MenuItem>
            </MenuList>
          </Menu>
          <Button width="12rem" colorScheme="blue">
            Add Contact
          </Button>
        </Stack>
      </Box>
    </Box>
  );
};
