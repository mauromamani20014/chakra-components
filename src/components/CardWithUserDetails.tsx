// https://pro.chakra-ui.com/components/application/cards

import { EditIcon } from '@chakra-ui/icons';
import { Box, Button, Flex, Heading, useColorModeValue } from '@chakra-ui/react';
import { FC } from 'react';

interface InfoProps {
  label: string;
  content: string;
}

export const CardWithUserDetails: FC = () => {
  const Header: FC = () => {
    return (
      <Flex
        direction="row"
        justify="space-between"
        align="center"
        py="4"
        px="5"
        borderBottom="1px"
        borderColor="gray.100">
        <Heading as="h2" fontSize="lg">
          Account Info
        </Heading>
        <Button
          colorScheme="whiteAlpha"
          textColor="gray.800"
          fontWeight="semibold"
          variant="outline"
          border="1px"
          borderColor="gray.100"
          leftIcon={<EditIcon />}>
          Edit
        </Button>
      </Flex>
    );
  };

  const Info: FC<InfoProps> = ({ label, content }) => {
    return (
      <Flex
        as="dl"
        direction={{ base: 'column', md: 'row' }}
        py="4"
        px="5"
        _even={{ bg: useColorModeValue('gray.50', 'gray.600') }}>
        <Box as="dt" minWidth="180px">
          {label}
        </Box>
        <Box as="dd" fontWeight="semibold">
          {content}
        </Box>
      </Flex>
    );
  };

  const info = [
    { label: 'Name', content: 'Chakra IU' },
    { label: 'Email', content: 'chakra-card@ad.com' },
    { label: 'Member since', content: 'February, 2021' },
    { label: 'Subscription plan', content: 'Starter Pro' },
  ];

  return (
    <Box bg="gray.100" py="10" px={{ base: '2', md: '8' }}>
      <Flex align="center" justify="center">
        <Box
          rounded="lg"
          bg="white"
          maxW="3xl"
          shadow="base"
          width={{ base: '100%', md: '60%' }}>
          <Header />
          {info.map((inf, idx) => (
            <Info key={idx} label={inf.label} content={inf.content} />
          ))}
        </Box>
      </Flex>
    </Box>
  );
};
